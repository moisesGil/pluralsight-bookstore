package com.mgil.courses.pluralsight.bookstore.util;

import com.mgil.courses.pluralsight.bookstore.qualifier.ThirteenDigits;

import java.util.Random;

@ThirteenDigits
public class IsbnGenerator implements NumberGenerator {

    @Override
    public String generateNumber() {
        return "13-771-" + Math.abs(new Random().nextInt());
    }
}
