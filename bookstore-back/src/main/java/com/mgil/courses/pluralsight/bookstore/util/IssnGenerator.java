package com.mgil.courses.pluralsight.bookstore.util;

import com.mgil.courses.pluralsight.bookstore.qualifier.EightDigits;

import javax.enterprise.inject.Default;
import java.util.Random;


@EightDigits
public class IssnGenerator implements NumberGenerator{

    private Random random = new Random();

    @Override
    public String generateNumber() {
        return  "8-756-" + Math.abs(random.nextInt());
    }
}
