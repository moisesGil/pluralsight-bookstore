import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'bs-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }



  delete() {
    console.log('Executing delete...');
    this.router.navigate(['/book-list']);
  }

}
