import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bs-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {



  books = [
    {
      id: 1,
      title: "Book 1",
      description: "This is the book 1",
      imageUrl: 'http://img.wowebook.info/images/5757722735.jpg'
    },
    {
      id: 2,
      title: "Book 2",
      description: "This is the book 2",
      imageUrl: 'http://img.wowebook.info/images/5757722735.jpg'

    },
    {
      id: 3,
      title: "Book 3",
      description: "This is the book 3",
      imageUrl: 'http://img.wowebook.info/images/5757722735.jpg'

    }
  ];

  bookQuantity: number =  3;


  constructor() { }

  ngOnInit() {
  }

}
